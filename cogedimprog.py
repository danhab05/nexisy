# from src.getcontact import ScrapContacts
# from flask import Flask, request
# from flask_cors import CORS
# from twilio.rest import Client
# import urllib.request
# import openpyxl
import pickle
import requests as r
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
from time import sleep
from typing_extensions import Self
import pandas as pdp
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from sympy import EX, false, true
from webdriver_manager.chrome import ChromeDriverManager
import pgeocode
from oauth2client.service_account import ServiceAccountCredentials
import gspread
from datetime import datetime
from datetime import date
from datetime import timedelta
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.firefox import GeckoDriverManager
from fake_useragent import UserAgent
import sys
import traceback
import os
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

app_creds_dictionary = {
    "type": "service_account",
    "project_id": "selogercopy",
    "private_key_id": "595a2272dfb920381c1c7893638d66608f439899",
    # trunk-ignore(gitleaks/private-key)
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDlceFwgFY2xK5x\nxB/QvGCQLYAGTco3vE9KyOLvFVVT7vYgUOmq24fSTC1Fwdn5oinn/LGS7wslq4Vg\nj1cHsHQGi5kI9kK1yKHktamuec8JM+QOZOioCw7l9bg7ugix0kk/YiWyVzIoQ0TQ\nR3qcMTu1FOQBcK/W/np2r5S3lK8BJOY5JQA4HqDTOFikDps3kGA3pwC6Bk/jYyKh\neP1bx1mCbXNFlfdCkE7HYpL/1G6uCX9VY0C6f5mKl/HtCzZpW4S28JwsnteXvb6/\n7j2PUBSv3AFa1PqiLPLDlT9vZ19kOKm1JiSV3/oRV0mki8iW6EQINgMStJw/Vti2\naB5dgcG/AgMBAAECggEAJrSMyq0jng6UvOVQVqaIhZnfWHDj9X4DRELaIWnQYJ3V\nkEMGAAgTg/LMylE2w6lK93MFw5smnr6t+8mCbW4jTH1dHglo3ICRauMIUkIDHnLN\n/G2iX8Kpyzolz5GE6PZn29TjNhAGios6fwYn1VRq1ZPGGMYZK6P3JbFd3ZAr632r\nqb8RBc7a21hZIcN8BnTAjJ72I04x98HC5PBKEE+Jh+51fsobzyQzESaN41lA3Mxn\nrExQyCo6GfyROEVGkZJHsWM22QFutCj+ZkwsBs1PgfG/kFZDwVvk5Ob0FnwnIMC4\nFxVetErV7s7ZlaEbnUQMjzwulpoJN+HPYR2VwOsLXQKBgQD+RJga/bafX7EAOG8b\nl/1eIRDryX/EIbTErTWI6DWaZok88phYdYTFocGQAL1MXBMBv1MwYb0HpH/7ic9A\ngWDX5hZruqG3QBHLC2UbsoRo2cIVjl24fuxa3wcxkwYK2vaFI/pqCMYAL9GQxFfP\n9ivWoXDwW6HhzKi0CsKwd61yxQKBgQDnAf+mWemm1m8xv1og2/VZ4wGZpsE3dWlw\nouaoV2vGRsPLr38AaKn/juGaq3idkPPIMJURyGfue5KUaD9hOcfpRaEvgJrwxDvI\njx00655/nT9jG4MxbYhwsc/xySe9KByBrnbFMvpRGCP7zvKJ4EuRzrj9ZMBGOv1a\nf4MLunCaswKBgQDSEdfdDNpRh9yTgwyArqy8lPMG6t3tK6/Ogg/lwXtvyeD+gqtN\nAiKkqURi9clvie6GFgzjvwxOgSDfr3aUI4/gL488h9/Np1hL+WkaXf25JpeL+agC\nopIhbXvjAEYQt+DOFPBxpDf5tYgdY6ns/cQwOgc1/W6cP9rtjKpW4zlgxQKBgQDM\nF5LzOKrasMcqr4GEWHDrCklGo/I++ie+0N1yS1I3qhugIBvXk6Dl3SlrCiWVKBSJ\ngIqjXGieZWS6Y2PjlTYO3Wqr2jaJkwWyiuZl8+ljm9z9xbfh+oC5+A7c8jh+wSp2\nVFownXpUHTzlYxAbSiPZ3UbI8LODGOjvhIqV2RFVSwKBgQC8UcbnPeBMj52fyRZV\nQngVJoprhHd17eLg3QUIkdziY+CTIg84kdhKzGP/gsBbpos3bCZoWzp9MGxUg3pw\nuk/69c1aWuaWmImAAze5BxowBYfUkkDj1I54pishhN5L1r8ytlZGxkBCuNLL0QSn\ntrKnpouBfLmVwU7txvjKVdR5Ww==\n-----END PRIVATE KEY-----\n",
    "client_email": "selogercopy@selogercopy.iam.gserviceaccount.com",
    "client_id": "111687988114084841090",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/selogercopy%40selogercopy.iam.gserviceaccount.com"
}
class Nexity:
    def __init__(self) -> None:
        self.driver = None

        

    def insertRowAtBegining(self, d):
        try:
           
            client = gspread.authorize(ServiceAccountCredentials.from_json_keyfile_dict(app_creds_dictionary, [
                'https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']))
            sheet = client.open('apimo_sonego_michael')
            # sheet = client.open('nexity')
            sheet_instance = sheet.get_worksheet(0)
            sheet_instance.insert_rows(d, 3)
        except Exception as e:
            print(e)
            pass
        

    def initDriver(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("user-data-dir=" + os.getcwd() + "/cogedim")
        chrome_options.add_experimental_option(
            'excludeSwitches', ['enable-logging'])
        self.driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
        self.email = "mickaelsonego@gmail.com"
        self.password = "Ladaba2024,"

    def getData(self):
        self.initDriver()
        self.driver.get("https://altarea-partenaires.com/")
        # wait page is charged and sendkeys to /html/body/div[3]/div/div[2]/form/div[2]/input
        WebDriverWait(self.driver, 200).until(EC.presence_of_element_located( (By.XPATH, '/html/body/header/div/div/div[2]/div/button') ))
        sleep(1)
        self.driver.find_element_by_xpath('/html/body/header/div/div/div[2]/div/button' ).click()
        sleep(1)
        a = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div[2]/div/form/div[1]/div/div[1]/div/div[1]/div[1]/input")
        a.click()
        a.send_keys(self.email)
        b = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div[2]/div/form/div[1]/div/div[1]/div/div[2]/div[1]/input")
        b.click()
        b.send_keys(self.password)
        self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div[2]/div/form/div[2]/div/button").click()
        # self.driver.find_element_by_xpath("/html/body/app-root/div/div[2]/app-homepage/div/app-fast-search-page/div/div[1]/app-fast-search-form/div/div[2]/button").click()
        # wait /html/body/div[1]/div[2]/div[4] is present
        sleep(15)
        ActionChains(self.driver).send_keys(Keys.ESCAPE).perform()
        
        sleep(0.5)
        while True:
            self.driver.get("https://altarea-partenaires.com/recherche")
            self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            self.driver.find_element_by_xpath("/html/body/main/div[1]/form/div[1]/div/div/div[1]/div[1]/div[1]/div/div/button").click()
            self.driver.find_element_by_xpath("/html/body/main/div[1]/form/div[1]/div/div/div[1]/div[1]/div[1]/div/div/div/div/input").send_keys("auvergne")
            sleep(1)
            self.driver.find_element_by_xpath("/html/body/main/div[1]/form/div[1]/div/div/div[1]/div[1]/div[1]/div/div/div/ul/li/span[1]").click()
            sleep(1)
            self.driver.find_element_by_xpath("/html/body/main/div[1]/form/div[1]/div/div/div[1]/div[2]/div[3]/button").click()
            pickle.dump(self.driver.get_cookies(), open("cogedim.pkl", "wb"))
            WebDriverWait(self.driver, 200).until(EC.presence_of_element_located( (By.XPATH, '/html/body/main/div/div[3]/div/div/table') ))

            dataFinal = []
            data = BeautifulSoup(self.driver.page_source, 'lxml').find("table")
            for row in data.find_all("tr")[1:]:
                try:
                    # write data to file using utf-8 encoding
                    ville = row.find_all("td")[0].get_text(strip=True)
                    zipCode = row.find_all("td")[1].get_text(strip=True)
                    metre = row.find_all("td")[6].get_text(strip=True)
                    type = row.find_all("td")[4].get_text(strip=True)
                    if "cave" in type:
                        pass
                    else:
                        if "Maison" in type:
                            type = "maison"
                        elif "Duplex" in type:
                            type = "Duplex"
                        else:
                            type = "appartement"

                        typologie = row.find_all("td")[1].find("div").get_text(strip=True)
                        if "1" in typologie:
                            typologie = "studio"
                        else:
                            typologie += " pièces"

                        etage = row.find_all("td")[4].find("div").get_text(strip=True).split("/")[0]
                        if "0" in etage:
                            etage   = "RDC"
                        elif "1" in etage:
                            etage  = "1er"
                        else:
                            etage  = etage + "ème"
                            etage = etage.replace(" ", "")


                        prix = row.find_all("td")[3].find("div").get_text(strip=True).split("€")[0].replace(" ", "") + "€"
                        nbrChambre = ""
                        if "2" in typologie:    
                            nbrChambre = "une chambres"
                        elif "3" in typologie:
                            nbrChambre = "2 chambres"
                        elif "4" in typologie:
                            nbrChambre = "3 chambres"
                        elif "5" in typologie:
                            nbrChambre = "4 chambres"
                        dateLivraison = row.find_all("td")[6].find("div").get_text(strip=True)
                        d = [ '', ville,  "", "", "une future belle résidence de standing", type, typologie, etage, metre, prix,
                                            "", "", "", "un balcon", "", nbrChambre, "", "une salle de bain avec wc", "", "*Nouvelle opportunité", dateLivraison, "Ok", "", "", ""]

                        dataFinal.append(d)
                    
                except Exception as e:                      
                    pass

            self.insertRowAtBegining(dataFinal)
            pickle.dump(self.driver.get_cookies(), open("cogedim.pkl", "wb"))
            dataFinal.clear()
            self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            sleep(1)
            self.driver.find_element_by_xpath("/html/body/app-root/div/div[2]/app-search-page/div/div/app-search-results/div/div/div/div/div[3]/app-search-results-lots/mat-paginator/div/div/div[2]/button[3]").click()
            sleep(6)


script = Nexity()
script.getData()
    

