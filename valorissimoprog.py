import pickle
import platform
import shutil
import requests as r
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
from time import sleep
from typing_extensions import Self
import pandas as pdp
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from sympy import EX, false, true
import urllib.request
from webdriver_manager.chrome import ChromeDriverManager
import pgeocode
from oauth2client.service_account import ServiceAccountCredentials
import gspread
from datetime import datetime
from datetime import date
from datetime import timedelta
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.firefox import GeckoDriverManager
from fake_useragent import UserAgent
import sys
import traceback
from os import path
import os
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

try:
    os.system("killall Google\ Chrome >/dev/null 2&>1")
    os.system("taskkill /F /IM chromedriver.exe")
    os.system("taskkill /F /IM chromedriver1.exe")
    for file in os.listdir(os.getcwd()):
        if "chromedriver" in file:
            try:
                os.remove(file)
            except:
                pass
except Exception:
    pass
try:
    os.removedirs(os.getcwd() + "/chromedriver-win32")
except:
    pass


app_creds_dictionary = {
    "type": "service_account",
    "project_id": "selogercopy",
    "private_key_id": "595a2272dfb920381c1c7893638d66608f439899",
    # trunk-ignore(gitleaks/private-key)
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDlceFwgFY2xK5x\nxB/QvGCQLYAGTco3vE9KyOLvFVVT7vYgUOmq24fSTC1Fwdn5oinn/LGS7wslq4Vg\nj1cHsHQGi5kI9kK1yKHktamuec8JM+QOZOioCw7l9bg7ugix0kk/YiWyVzIoQ0TQ\nR3qcMTu1FOQBcK/W/np2r5S3lK8BJOY5JQA4HqDTOFikDps3kGA3pwC6Bk/jYyKh\neP1bx1mCbXNFlfdCkE7HYpL/1G6uCX9VY0C6f5mKl/HtCzZpW4S28JwsnteXvb6/\n7j2PUBSv3AFa1PqiLPLDlT9vZ19kOKm1JiSV3/oRV0mki8iW6EQINgMStJw/Vti2\naB5dgcG/AgMBAAECggEAJrSMyq0jng6UvOVQVqaIhZnfWHDj9X4DRELaIWnQYJ3V\nkEMGAAgTg/LMylE2w6lK93MFw5smnr6t+8mCbW4jTH1dHglo3ICRauMIUkIDHnLN\n/G2iX8Kpyzolz5GE6PZn29TjNhAGios6fwYn1VRq1ZPGGMYZK6P3JbFd3ZAr632r\nqb8RBc7a21hZIcN8BnTAjJ72I04x98HC5PBKEE+Jh+51fsobzyQzESaN41lA3Mxn\nrExQyCo6GfyROEVGkZJHsWM22QFutCj+ZkwsBs1PgfG/kFZDwVvk5Ob0FnwnIMC4\nFxVetErV7s7ZlaEbnUQMjzwulpoJN+HPYR2VwOsLXQKBgQD+RJga/bafX7EAOG8b\nl/1eIRDryX/EIbTErTWI6DWaZok88phYdYTFocGQAL1MXBMBv1MwYb0HpH/7ic9A\ngWDX5hZruqG3QBHLC2UbsoRo2cIVjl24fuxa3wcxkwYK2vaFI/pqCMYAL9GQxFfP\n9ivWoXDwW6HhzKi0CsKwd61yxQKBgQDnAf+mWemm1m8xv1og2/VZ4wGZpsE3dWlw\nouaoV2vGRsPLr38AaKn/juGaq3idkPPIMJURyGfue5KUaD9hOcfpRaEvgJrwxDvI\njx00655/nT9jG4MxbYhwsc/xySe9KByBrnbFMvpRGCP7zvKJ4EuRzrj9ZMBGOv1a\nf4MLunCaswKBgQDSEdfdDNpRh9yTgwyArqy8lPMG6t3tK6/Ogg/lwXtvyeD+gqtN\nAiKkqURi9clvie6GFgzjvwxOgSDfr3aUI4/gL488h9/Np1hL+WkaXf25JpeL+agC\nopIhbXvjAEYQt+DOFPBxpDf5tYgdY6ns/cQwOgc1/W6cP9rtjKpW4zlgxQKBgQDM\nF5LzOKrasMcqr4GEWHDrCklGo/I++ie+0N1yS1I3qhugIBvXk6Dl3SlrCiWVKBSJ\ngIqjXGieZWS6Y2PjlTYO3Wqr2jaJkwWyiuZl8+ljm9z9xbfh+oC5+A7c8jh+wSp2\nVFownXpUHTzlYxAbSiPZ3UbI8LODGOjvhIqV2RFVSwKBgQC8UcbnPeBMj52fyRZV\nQngVJoprhHd17eLg3QUIkdziY+CTIg84kdhKzGP/gsBbpos3bCZoWzp9MGxUg3pw\nuk/69c1aWuaWmImAAze5BxowBYfUkkDj1I54pishhN5L1r8ytlZGxkBCuNLL0QSn\ntrKnpouBfLmVwU7txvjKVdR5Ww==\n-----END PRIVATE KEY-----\n",
    "client_email": "selogercopy@selogercopy.iam.gserviceaccount.com",
    "client_id": "111687988114084841090",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/selogercopy%40selogercopy.iam.gserviceaccount.com",
}


class Nexity:
    def __init__(self) -> None:
        self.driver = None
        self.user = ""
        if "mickaelsonego" in str(os.getcwd()):
            self.user = "michael"
        else:
            self.user = "lesslie"

    def insertRowAtBegining(self, d):
        try:

            client = gspread.authorize(
                ServiceAccountCredentials.from_json_keyfile_dict(
                    app_creds_dictionary,
                    [
                        "https://spreadsheets.google.com/feeds",
                        "https://www.googleapis.com/auth/drive",
                    ],
                )
            )

            if self.user == "michael":
                sheet = client.open("apimo_sonego_michael2")
            else:
                sheet = client.open("apimo_Haddad _Lesslie 2")

            # sheet = client.open('nexity')
            sheet_instance = sheet.get_worksheet(0)
            sheet_instance.insert_rows(d, 2)
        except Exception as e:
            print(e)
            pass

    def initDriver(self):
        chrome_options = webdriver.ChromeOptions()
        try:
            chrome_options.add_argument("user-data-dir=" + os.getcwd() + "/valorissimo")
        except:
            os.makedirs(os.getcwd() + "/valorissimo")
            chrome_options.add_argument("user-data-dir=" + os.getcwd() + "/valorissimo")

        chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
        try:
            self.driver = webdriver.Chrome(
                ChromeDriverManager().install(), options=chrome_options
            )
        except:
            VER_CHROME = "127.0.6533.4"
            driverFile = urllib.request.urlretrieve(
                f"https://edgedl.me.gvt1.com/edgedl/chrome/chrome-for-testing/{VER_CHROME}/win32/chromedriver-win32.zip",
                os.getcwd() + "/chromedriver_win32.zip",
            )
            shutil.unpack_archive(os.getcwd() + "/chromedriver_win32.zip", os.getcwd())
            os.chmod(os.getcwd() + "/chromedriver-win32/chromedriver.exe", 754)
            os.rename(
                os.getcwd() + "/chromedriver-win32/chromedriver.exe", "chromedriver.exe"
            )
            self.driver = webdriver.Chrome(
                os.getcwd() + "/chromedriver.exe", options=chrome_options
            )
            # driverFile = urllib.request.urlretrieve(
            #                 f"https://edgedl.me.gvt1.com/edgedl/chrome/chrome-for-testing/{VER_CHROME}/mac-arm64/chromedriver-mac-arm64.zip", os.getcwd() + "/chromedriver_mac_arm64.zip")
            # shutil.unpack_archive(
            #     os.getcwd() + "/chromedriver_mac_arm64.zip", os.getcwd())

            # os.chmod(os.getcwd() + "/chromedriver-mac-arm64/chromedriver", 754)
            # os.rename(os.getcwd() + "/chromedriver-mac-arm64/chromedriver", "chromedriver")
            # self.driver = webdriver.Chrome(
            #     os.getcwd() + "/chromedriver", options=chrome_options)

        self.email = "mickaelsonego@gmail.com"
        self.password = "Teamblg2024,"

    def getData(self):
        self.initDriver()
        self.driver.get("https://www.valorissimo.com/login")
        sleep(2)
        if "homepage" not in self.driver.current_url:
            # wait page is charged and sendkeys to /html/body/div[3]/div/div[2]/form/div[2]/input
            WebDriverWait(self.driver, 200).until(
                EC.presence_of_element_located(
                    (
                        By.XPATH,
                        "/html/body/app-root/div/div[2]/app-log-container/div/app-log-account/div/form/mat-form-field[1]",
                    )
                )
            )
            sleep(1)
            a = self.driver.find_element_by_xpath(
                "/html/body/app-root/div/div[2]/app-log-container/div/app-log-account/div/form/mat-form-field[1]/div/div[1]/div/input"
            )
            a.click()
            a.send_keys(self.email)
            b = self.driver.find_element_by_xpath(
                "/html/body/app-root/div/div[2]/app-log-container/div/app-log-account/div/form/mat-form-field[2]/div/div[1]/div/input"
            )
            b.click()
            b.send_keys(self.password)
            self.driver.find_element_by_xpath(
                "/html/body/app-root/div/div[2]/app-log-container/div/app-log-account/div/form/div[2]/button"
            ).click()
            sleep(1)
        else:
            sleep(5)
        sleep(60)
        if self.user == "michael":
            self.driver.execute_script(
                f"""window.open("https://docs.google.com/spreadsheets/d/119jq-jH7-Ic02EARt7h4eAIL0R9DlXgDnQ6DqVoBMqs/edit?usp=sharing","_blank");"""
            )
        else:
            self.driver.execute_script(
                """window.open("https://docs.google.com/spreadsheets/d/1apUmzufLTuZ60ENHxI4Kll1P753eRRqPyxtKHnE_Hyg/edit?usp=sharing","_blank");"""
            )
        try:
            self.driver.find_element_by_xpath(
                "/html/body/app-root/div/div[2]/app-homepage/div/app-fast-search-page/div/div[1]/app-fast-search-form/div/div[2]/button"
            ).click()
        # wait /html/body/div[1]/div[2]/div[4] is present
        except:
            pass
        try:
            WebDriverWait(self.driver, 200).until(
                EC.presence_of_element_located(
                    (
                        By.XPATH,
                        "/html/body/app-root/div/div[2]/app-search-page/div/div/app-search-results/div/div/div/div/div[2]/button[3]",
                    )
                )
            )
        except:
            pass
        # self.driver.find_element_by_xpath(
        #     "/html/body/app-root/div/div[2]/app-search-page/div/app-search-fix-menu/div/div/app-filter-menu-badge[3]/div").click()
        # sleep(0.5)
        # self.driver.find_element_by_xpath(
        #     "/html/body/div[3]/div[2]/div/mat-dialog-container/app-search-filter-dialog/mat-dialog-content/app-search-filter-content/div[5]/app-search-filter-outside/div[2]/app-checkbox-populated/mat-checkbox[2]/label/span[1]").click()
        # self.driver.find_element_by_xpath(
        #     "/html/body/div[3]/div[2]/div/mat-dialog-container/app-search-filter-dialog/mat-dialog-actions").click()
        # sleep(3)
        self.driver.find_element_by_xpath(
            "/html/body/app-root/div/div[2]/app-search-page/div/div/app-search-results/div/div/div/div/div[2]/button[3]"
        ).click()
        sleep(6)
        while True:
            dataFinal = []
            data = BeautifulSoup(self.driver.page_source, "lxml").find("table")
            for row in data.find_all("tr")[1:]:
                try:
                    # write data to file using utf-8 encoding
                    ville = (
                        row.find_all("td")[0]
                        .find_all("div")[0]
                        .find_all("div")[3]
                        .get_text(strip=True)
                    )
                    type = ville.split("- ")[1]
                    ville = ville.split("\n")[0]

                    typologie = row.find_all("td")[1].find("div").get_text(strip=True)
                    if "1" in typologie:
                        typologie = "studio"
                    else:
                        typologie += " pièces"

                    etage = (
                        row.find_all("td")[4]
                        .find("div")
                        .get_text(strip=True)
                        .split("/")[0]
                    )
                    if "0" in etage:
                        etage = "RDC"
                    elif "1" in etage:
                        etage = "1er"
                    else:
                        etage = etage + "ème"
                        etage = etage.replace(" ", "")

                    metre = (
                        row.find_all("td")[2]
                        .find("div")
                        .get_text(strip=True)
                        .split("/")[0]
                        .replace("m²", "")
                        .replace(".", ",")
                    )
                    prix = (
                        row.find_all("td")[3]
                        .find("div")
                        .get_text(strip=True)
                        .split("€")[0]
                        .replace(" ", "")
                        + "€"
                    )
                    nbrChambre = ""
                    if "2" in typologie:
                        nbrChambre = "une chambres"
                    elif "3" in typologie:
                        nbrChambre = "2 chambres"
                    elif "4" in typologie:
                        nbrChambre = "3 chambres"
                    elif "5" in typologie:
                        nbrChambre = "4 chambres"
                    dateLivraison = (
                        row.find_all("td")[7].find("div").get_text(strip=True)
                    )
                    d = [
                        "",
                        ville,
                        "",
                        "",
                        "une future belle résidence de standing",
                        type,
                        typologie,
                        etage,
                        metre,
                        prix,
                        "",
                        "",
                        "",
                        "un balcon",
                        "",
                        nbrChambre,
                        "",
                        "une salle de bain avec wc",
                        "",
                        "*Nouvelle opportunité",
                        dateLivraison,
                        "Ok",
                        "",
                        "",
                        "",
                    ]

                    dataFinal.append(d)

                except Exception as e:
                    pass

            self.insertRowAtBegining(dataFinal)
            try:
                pickle.dump(
                    self.driver.get_cookies(), open("valorissimoprog.pkl", "wb")
                )
            except Exception:
                open("valorissimoprog.pkl", "wb")
                pickle.dump(
                    self.driver.get_cookies(), open("valorissimoprog.pkl", "wb")
                )
            dataFinal.clear()
            self.driver.execute_script(
                "window.scrollTo(0, document.body.scrollHeight);"
            )
            sleep(1)
            self.driver.find_element_by_xpath(
                "/html/body/app-root/div/div[2]/app-search-page/div/div/app-search-results/div/div/div/div/div[3]/app-search-results-lots/mat-paginator/div/div/div[2]/button[3]"
            ).click()
            sleep(6)


script = Nexity()
script.getData()
