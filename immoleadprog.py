import pickle
import shutil
import requests as r
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
from time import sleep
from typing_extensions import Self
import pandas as pdp
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from sympy import EX, false, true
import urllib.request
from webdriver_manager.chrome import ChromeDriverManager
import pgeocode
from oauth2client.service_account import ServiceAccountCredentials
import gspread
from datetime import datetime
from datetime import date
from datetime import timedelta
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.firefox import GeckoDriverManager
from fake_useragent import UserAgent
import sys
import traceback
from os import path
import os
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

try:
    os.system("taskkill /F /IM chromedriver.exe")
    os.system("taskkill /F /IM chromedriver1.exe")
    for file in os.listdir(os.getcwd()):
        if "chromedriver" in file:
            try:
                os.remove(file)
            except:
                pass
except Exception:
    pass
try:
    os.removedirs(os.getcwd() + "/chromedriver-win32")
except:
    pass


app_creds_dictionary = {
    "type": "service_account",
    "project_id": "selogercopy",
    "private_key_id": "595a2272dfb920381c1c7893638d66608f439899",
    # trunk-ignore(gitleaks/private-key)
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDlceFwgFY2xK5x\nxB/QvGCQLYAGTco3vE9KyOLvFVVT7vYgUOmq24fSTC1Fwdn5oinn/LGS7wslq4Vg\nj1cHsHQGi5kI9kK1yKHktamuec8JM+QOZOioCw7l9bg7ugix0kk/YiWyVzIoQ0TQ\nR3qcMTu1FOQBcK/W/np2r5S3lK8BJOY5JQA4HqDTOFikDps3kGA3pwC6Bk/jYyKh\neP1bx1mCbXNFlfdCkE7HYpL/1G6uCX9VY0C6f5mKl/HtCzZpW4S28JwsnteXvb6/\n7j2PUBSv3AFa1PqiLPLDlT9vZ19kOKm1JiSV3/oRV0mki8iW6EQINgMStJw/Vti2\naB5dgcG/AgMBAAECggEAJrSMyq0jng6UvOVQVqaIhZnfWHDj9X4DRELaIWnQYJ3V\nkEMGAAgTg/LMylE2w6lK93MFw5smnr6t+8mCbW4jTH1dHglo3ICRauMIUkIDHnLN\n/G2iX8Kpyzolz5GE6PZn29TjNhAGios6fwYn1VRq1ZPGGMYZK6P3JbFd3ZAr632r\nqb8RBc7a21hZIcN8BnTAjJ72I04x98HC5PBKEE+Jh+51fsobzyQzESaN41lA3Mxn\nrExQyCo6GfyROEVGkZJHsWM22QFutCj+ZkwsBs1PgfG/kFZDwVvk5Ob0FnwnIMC4\nFxVetErV7s7ZlaEbnUQMjzwulpoJN+HPYR2VwOsLXQKBgQD+RJga/bafX7EAOG8b\nl/1eIRDryX/EIbTErTWI6DWaZok88phYdYTFocGQAL1MXBMBv1MwYb0HpH/7ic9A\ngWDX5hZruqG3QBHLC2UbsoRo2cIVjl24fuxa3wcxkwYK2vaFI/pqCMYAL9GQxFfP\n9ivWoXDwW6HhzKi0CsKwd61yxQKBgQDnAf+mWemm1m8xv1og2/VZ4wGZpsE3dWlw\nouaoV2vGRsPLr38AaKn/juGaq3idkPPIMJURyGfue5KUaD9hOcfpRaEvgJrwxDvI\njx00655/nT9jG4MxbYhwsc/xySe9KByBrnbFMvpRGCP7zvKJ4EuRzrj9ZMBGOv1a\nf4MLunCaswKBgQDSEdfdDNpRh9yTgwyArqy8lPMG6t3tK6/Ogg/lwXtvyeD+gqtN\nAiKkqURi9clvie6GFgzjvwxOgSDfr3aUI4/gL488h9/Np1hL+WkaXf25JpeL+agC\nopIhbXvjAEYQt+DOFPBxpDf5tYgdY6ns/cQwOgc1/W6cP9rtjKpW4zlgxQKBgQDM\nF5LzOKrasMcqr4GEWHDrCklGo/I++ie+0N1yS1I3qhugIBvXk6Dl3SlrCiWVKBSJ\ngIqjXGieZWS6Y2PjlTYO3Wqr2jaJkwWyiuZl8+ljm9z9xbfh+oC5+A7c8jh+wSp2\nVFownXpUHTzlYxAbSiPZ3UbI8LODGOjvhIqV2RFVSwKBgQC8UcbnPeBMj52fyRZV\nQngVJoprhHd17eLg3QUIkdziY+CTIg84kdhKzGP/gsBbpos3bCZoWzp9MGxUg3pw\nuk/69c1aWuaWmImAAze5BxowBYfUkkDj1I54pishhN5L1r8ytlZGxkBCuNLL0QSn\ntrKnpouBfLmVwU7txvjKVdR5Ww==\n-----END PRIVATE KEY-----\n",
    "client_email": "selogercopy@selogercopy.iam.gserviceaccount.com",
    "client_id": "111687988114084841090",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/selogercopy%40selogercopy.iam.gserviceaccount.com",
}


class Nexity:
    def __init__(self) -> None:
        self.driver = None

    def insertRowAtBegining(self, d):
        try:

            client = gspread.authorize(
                ServiceAccountCredentials.from_json_keyfile_dict(
                    app_creds_dictionary,
                    [
                        "https://spreadsheets.google.com/feeds",
                        "https://www.googleapis.com/auth/drive",
                    ],
                )
            )
            sheet = client.open("apimo_sonego_michael")
            # sheet = client.open('nexity')
            sheet_instance = sheet.get_worksheet(0)
            sheet_instance.insert_rows(d, 2)
        except Exception as e:
            print(e)
            pass

    def initDriver(self):
        chrome_options = webdriver.ChromeOptions()
        try:
            chrome_options.add_argument("user-data-dir=" + os.getcwd() + "/immolead")
        except:
            os.makedirs(os.getcwd() + "/immolead")
            chrome_options.add_argument("user-data-dir=" + os.getcwd() + "/immolead")

        chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
        try:
            self.driver = webdriver.Chrome(
                ChromeDriverManager().install(), options=chrome_options
            )
        except:
            VER_CHROME = "122.0.6261.57"
            driverFile = urllib.request.urlretrieve(
                f"https://edgedl.me.gvt1.com/edgedl/chrome/chrome-for-testing/{VER_CHROME}/win32/chromedriver-win32.zip",
                os.getcwd() + "/chromedriver_win32.zip",
            )
            shutil.unpack_archive(os.getcwd() + "/chromedriver_win32.zip", os.getcwd())
            os.chmod(os.getcwd() + "/chromedriver-win32/chromedriver.exe", 754)
            os.rename(
                os.getcwd() + "/chromedriver-win32/chromedriver.exe", "chromedriver.exe"
            )
            self.driver = webdriver.Chrome(
                os.getcwd() + "/chromedriver.exe", options=chrome_options
            )
            # driverFile = urllib.request.urlretrieve(
            #                 f"https://edgedl.me.gvt1.com/edgedl/chrome/chrome-for-testing/{VER_CHROME}/mac-arm64/chromedriver-mac-arm64.zip", os.getcwd() + "/chromedriver_mac_arm64.zip")
            # shutil.unpack_archive(
            #     os.getcwd() + "/chromedriver_mac_arm64.zip", os.getcwd())

            # os.chmod(os.getcwd() + "/chromedriver-mac-arm64/chromedriver", 754)
            # os.rename(os.getcwd() + "/chromedriver-mac-arm64/chromedriver", "chromedriver")
            # self.driver = webdriver.Chrome(
            #     os.getcwd() + "/chromedriver", options=chrome_options)

        self.email = "mickaelsonego@gmail.com"
        self.password = "Ladaba2024,"

    def getData(self):
        self.initDriver()
        self.driver.get("https://immo-lead.com/")
        sleep(2)
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[2]/div[2]/form/div[2]/input"
        ).send_keys(self.email)
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[2]/div[2]/form/div[3]/input"
        ).send_keys(self.password)
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[2]/div[2]/form/div[5]/button"
        ).click()

        sleep(30)
        # # moi
        # self.driver.find_element_by_xpath("/html/body/div/div/div/div[2]/div[2]/div[2]/form/div[5]/button").click()
        # self.driver.find_element_by_xpath("/html/body/div[4]/div[9]/div[2]/div[1]/div/p[4]/span/a").click()
        # # moi
        if "appli" in self.driver.current_url:
            sleep(2)
        self.driver.execute_script(
            f"""window.open("https://docs.google.com/spreadsheets/d/119jq-jH7-Ic02EARt7h4eAIL0R9DlXgDnQ6DqVoBMqs/edit?usp=sharing","_blank");"""
        )

        for i in range(3, 7):
            self.driver.execute_script("window.scrollTo(0, 0)")
            self.driver.find_element_by_xpath(
                f"/html/body/div[4]/div[9]/div[2]/div[1]/div/p[{str(i)}]/span/a"
            ).click()
            sleep(5)
            dataFinal = []
            data = BeautifulSoup(self.driver.page_source, "lxml").find_all("tr")
            # del data[0]
            data = data[1:]
            for row in data:
                td = BeautifulSoup(str(row), "lxml").find_all("td")
                try:
                    type = str(td[3].get_text(strip=True))
                    typologie = str(td[4].get_text(strip=True))
                    ville = str(td[13].get_text(strip=True))
                    metre = str(td[12].get_text(strip=True))
                    prix = str(td[14].get_text(strip=True))
                    try:
                        print("herze")
                        prix2 = prix.split("€")[0]
                        prix = prix2
                    except Exception:
                        prix2 = prix.split("%")[1]
                        prix = prix2

                    print("herze 2")
                    etage = str(td[7].get_text(strip=True))
                    nbrChambre = ""
                    if "Strudio" in typologie:
                        typologie = "Studio"
                    if "2" in typologie:
                        nbrChambre = "une chambres"
                    elif "3" in typologie:
                        nbrChambre = "2 chambres"
                    elif "4" in typologie:
                        nbrChambre = "3 chambres"
                    elif "5" in typologie:
                        nbrChambre = "4 chambres"
                    # dateLivraison = row.find_all("td")[6].find(
                    #     "div").get_text(strip=True)
                    print(type, typologie, ville, metre, prix, etage, nbrChambre)
                    d = [
                        "",
                        ville,
                        "",
                        "",
                        "une future belle résidence de standing",
                        type,
                        typologie,
                        etage,
                        metre,
                        prix,
                        "",
                        "",
                        "",
                        "un balcon",
                        "",
                        nbrChambre,
                        "",
                        "une salle de bain avec wc",
                        "",
                        "*Nouvelle opportunité",
                        "",
                        "Ok",
                        "",
                        "",
                        "",
                    ]
                    print(d)
                    dataFinal.append(d)
                except Exception as e:
                    print(e)
                    pass

            self.insertRowAtBegining(dataFinal)
            try:
                pickle.dump(self.driver.get_cookies(), open("immolead.pkl", "wb"))
            except Exception:
                open("immolead.pkl", "wb")
                pickle.dump(self.driver.get_cookies(), open("immolead.pkl", "wb"))
            dataFinal.clear()
        self.driver.quit()


script = Nexity()
script.getData()
